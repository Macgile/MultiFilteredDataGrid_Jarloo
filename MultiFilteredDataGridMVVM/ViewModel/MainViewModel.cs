﻿using Common;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MultiFilteredDataGridMVVM.Helpers;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UsePatternMatching
// ReSharper disable UseNameofExpression

namespace MultiFilteredDataGridMVVM.ViewModel
{
    public class ColumnFilter : ViewModelBase
    {
        #region Private Fields

        private bool canRemoveFilter;
        private ObservableCollection<string> itemColumn;
        private string selectedItem;
        private ICommand removeFilterCommand;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Remove filter Command
        /// </summary>
        public ICommand RemoveFilterCommand
            => removeFilterCommand ??
               (removeFilterCommand = new RelayCommand(RemoveFilter, () => canRemoveFilter));

        public string FieldName { get; set; }

        public ObservableCollection<string> ItemColumn
        {
            get => itemColumn;
            set
            {
                if (itemColumn == value) return;
                itemColumn = value;
                RaisePropertyChanged("ItemColumn");
            }
        }

        public string SelectedItem
        {
            get => selectedItem;
            set
            {
                if (selectedItem == value) return;
                selectedItem = value;
                RaisePropertyChanged("SelectedItem");
                if(selectedItem != null)
                AddFilter();
            }
        }

        #endregion Public Properties

        #region Private Properties

        /// <summary>
        /// Global CollectionViewSource (shared by all filter)
        /// </summary>
        public CollectionViewSource ViewSource { get; set; }

        #endregion Private Properties

        #region Public Methods

        public void AddFilter()
        {
            Debug.WriteLine($"canRemoveFilter:{canRemoveFilter}");

            // see Notes on Adding Filters:
            if (canRemoveFilter)
            {
                Debug.WriteLine("CanRemoveFilter");
                ViewSource.Filter -= Filter;
                ViewSource.Filter += Filter;
            }
            else
            {
                Debug.WriteLine("AddFilter");
                ViewSource.Filter += Filter;
                canRemoveFilter = true;
            }
        }

        /// <summary>
        /// Remove Filter
        /// </summary>
        public void RemoveFilter()
        {
            Debug.WriteLine("RemoveFilter");
            ViewSource.Filter -= Filter;
            SelectedItem = null;
            canRemoveFilter = false;
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Filter By item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Filter(object sender, FilterEventArgs e)
        {
            // see Notes on Filter Methods:
            var src = e.Item as Thing;
            if (src == null)
                e.Accepted = false;
            // get field by FieldName
            else if (string.CompareOrdinal(SelectedItem, src.Author) != 0)
                e.Accepted = false;
        }

        #endregion Private Methods
    }

    public class MainViewModel : ViewModelBase
    {
        #region Members

        private ObservableCollection<string> _authors;
        private bool _canCanRemoveAuthorFilter;
        private bool _canCanRemoveCountryFilter;
        private bool _canCanRemoveYearFilter;
        private ObservableCollection<string> _countries;
        private string _selectedAuthor;
        private string _selectedCountry;
        private Nullable<int> _selectedYear;
        private ObservableCollection<Thing> _things;
        private ObservableCollection<int> _years;

        #endregion Members

        #region Private Enums

        private enum FilterField
        {
            Author,
            Country,
            Year,
            None
        }

        #endregion Private Enums

        #region Internal Properties

        /// <summary>
        /// Gets or sets the IDownloadDataService member
        /// </summary>
        internal IDataService DataService { get; set; }

        #endregion Internal Properties

        #region Private Properties

        /// <summary>
        /// Gets or sets the CollectionViewSource which is the proxy for the
        /// collection of Things and the datagrid in which each thing is displayed.
        /// </summary>
        private CollectionViewSource CVS { get; set; }

        #endregion Private Properties

        #region Public Constructors

        public MainViewModel(IDataService dataService)
        {
            InitializeCommands();
            DataService = dataService;
            LoadData();

            //--------------------------------------------------------------
            // This 'registers' the instance of this view model to recieve messages with this type of token.  This
            // is used to recieve a reference from the view that the collectionViewSource has been instantiated
            // and to recieve a reference to the CollectionViewSource which will be used in the view model for
            // filtering
            Messenger.Default.Register<ViewCollectionViewSourceMessageToken>(this,
                Handle_ViewCollectionViewSourceMessageToken);
        }

        #endregion Public Constructors

        #region Public Methods

        public void AddAuthorFilter()
        {
            Debug.WriteLine($"CanRemoveAuthorFilter:{CanRemoveAuthorFilter}");

            ColumnFilter.AddFilter();

            // see Notes on Adding Filters:
            if (CanRemoveAuthorFilter)
            {
                Debug.WriteLine("CanRemoveAuthorFilter");
                CVS.Filter -= new FilterEventHandler(FilterByAuthor);
                CVS.Filter += new FilterEventHandler(FilterByAuthor);
            }
            else
            {
                Debug.WriteLine("AddAuthorFilter");

                

                CVS.Filter += new FilterEventHandler(FilterByAuthor);
                CanRemoveAuthorFilter = true;
            }
        }

        public void AddCountryFilter()
        {
            Debug.WriteLine($"CanRemoveCountryFilter:{CanRemoveCountryFilter}");
            // see Notes on Adding Filters:
            if (CanRemoveCountryFilter)
            {
                Debug.WriteLine("CanRemoveCountryFilter");
                CVS.Filter -= new FilterEventHandler(FilterByCountry);
                CVS.Filter += new FilterEventHandler(FilterByCountry);
            }
            else
            {
                Debug.WriteLine("AddCountryFilter");
                CVS.Filter += new FilterEventHandler(FilterByCountry);
                CanRemoveCountryFilter = true;
            }
        }

        public void AddYearFilter()
        {
            Debug.WriteLine($"CanRemoveYearFilter:{CanRemoveYearFilter}");
            // see Notes on Adding Filters:
            if (CanRemoveYearFilter)
            {
                Debug.WriteLine("CanRemoveYearFilter");
                CVS.Filter -= new FilterEventHandler(FilterByYear);
                CVS.Filter += new FilterEventHandler(FilterByYear);
            }
            else
            {
                Debug.WriteLine("AddYearFilter");
                CVS.Filter += new FilterEventHandler(FilterByYear);
                CanRemoveYearFilter = true;
            }
        }

        public override void Cleanup()
        {
            Debug.WriteLine("Cleanup");
            Messenger.Default.Unregister<ViewCollectionViewSourceMessageToken>(this);
            base.Cleanup();
        }

        #endregion Public Methods

        #region Properties (Displayable in View)

        public ColumnFilter ColumnFilter { get; set; }


        /// <summary>
        /// Gets or sets a list of authors which is used to populate the author filter
        /// drop down list.
        /// </summary>
        public ObservableCollection<string> Authors
        {
            get { return _authors; }
            set
            {
                if (_authors == value) return;
                _authors = value;
                RaisePropertyChanged("Authors");
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating if the Author filter, if applied, can be removed.
        /// </summary>
        public bool CanRemoveAuthorFilter
        {
            get { return _canCanRemoveAuthorFilter; }
            set
            {
                _canCanRemoveAuthorFilter = value;
                RaisePropertyChanged("CanRemoveAuthorFilter");
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating if the Country filter, if applied, can be removed.
        /// </summary>
        public bool CanRemoveCountryFilter
        {
            get { return _canCanRemoveCountryFilter; }
            set
            {
                _canCanRemoveCountryFilter = value;
                RaisePropertyChanged("CanRemoveCountryFilter");
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating if the Year filter, if applied, can be removed.
        /// </summary>
        public bool CanRemoveYearFilter
        {
            get { return _canCanRemoveYearFilter; }
            set
            {
                _canCanRemoveYearFilter = value;
                RaisePropertyChanged("CanRemoveYearFilter");
            }
        }

        /// <summary>
        /// Gets or sets a list of authors which is used to populate the country filter
        /// drop down list.
        /// </summary>
        public ObservableCollection<string> Countries
        {
            get { return _countries; }
            set
            {
                if (_countries == value)
                    return;
                _countries = value;
                RaisePropertyChanged("Countries");
            }
        }

        /// <summary>
        /// Gets or sets the selected author in the list authors to filter the collection
        /// </summary>
        public string SelectedAuthor
        {
            get { return _selectedAuthor; }
            set
            {
                if (_selectedAuthor == value)
                    return;
                _selectedAuthor = value;
                RaisePropertyChanged("SelectedAuthor");
                ApplyFilter(!string.IsNullOrEmpty(_selectedAuthor) ? FilterField.Author : FilterField.None);
            }
        }

        // Filter properties =============
        /// <summary>
        /// Gets or sets the selected author in the list countries to filter the collection
        /// </summary>
        public string SelectedCountry
        {
            get { return _selectedCountry; }
            set
            {
                if (_selectedCountry == value)
                    return;
                _selectedCountry = value;
                RaisePropertyChanged("SelectedCountry");
                ApplyFilter(!string.IsNullOrEmpty(_selectedCountry) ? FilterField.Country : FilterField.None);
            }
        }

        /// <summary>
        /// Gets or sets the selected author in the list years to filter the collection
        /// </summary>
        public Nullable<int> SelectedYear
        {
            get { return _selectedYear; }
            set
            {
                if (_selectedYear == value)
                    return;
                _selectedYear = value;
                RaisePropertyChanged("SelectedYear");
                ApplyFilter(_selectedYear.HasValue ? FilterField.Year : FilterField.None);
            }
        }

        /// <summary>
        /// Gets or sets the primary collection of Thing objects to be displayed
        /// </summary>
        public ObservableCollection<Thing> Things
        {
            get { return _things; }
            set
            {
                if (_things == value)
                    return;
                _things = value;
                RaisePropertyChanged("Things");
            }
        }

        /// <summary>
        /// Gets or sets a list of authors which is used to populate the year filter
        /// drop down list.
        /// </summary>
        public ObservableCollection<int> Years
        {
            get { return _years; }
            set
            {
                if (_years == value)
                    return;
                _years = value;
                RaisePropertyChanged("Years");
            }
        }

        #endregion Properties (Displayable in View)

        #region Commands

        public ICommand RemoveAuthorFilterCommand { get; private set; }
        public ICommand RemoveCountryFilterCommand { get; private set; }
        public ICommand RemoveYearFilterCommand { get; private set; }
        public ICommand ResetFiltersCommand { get; private set; }

        #endregion Commands

        /// <summary>
        /// Remove Author Filter
        /// </summary>
        public void RemoveAuthorFilter()
        {
            Debug.WriteLine("RemoveAuthorFilter");
            CVS.Filter -= new FilterEventHandler(FilterByAuthor);
            SelectedAuthor = null;
            CanRemoveAuthorFilter = false;
        }

        /// <summary>
        /// Remove Country Filter
        /// </summary>
        public void RemoveCountryFilter()
        {
            Debug.WriteLine("RemoveCountryFilter");
            CVS.Filter -= new FilterEventHandler(FilterByCountry);
            SelectedCountry = null;
            CanRemoveCountryFilter = false;
        }

        /// <summary>
        /// Remove Year Filter
        /// </summary>
        public void RemoveYearFilter()
        {
            Debug.WriteLine("RemoveYearFilter");
            CVS.Filter -= new FilterEventHandler(FilterByYear);
            SelectedYear = null;
            CanRemoveYearFilter = false;
        }

        /// <summary>
        ///  Remove All Filter
        /// </summary>
        public void ResetFilters()
        {
            Debug.WriteLine("ResetFilters");
            // clear filters
            RemoveYearFilter();
            RemoveAuthorFilter();
            RemoveCountryFilter();
        }

        /// <summary>
        /// Apply Filter by FilterField enum
        /// </summary>
        /// <param name="field"></param>
        private void ApplyFilter(FilterField field)
        {
            Debug.WriteLine($"ApplyFilter Field : {field}");

            switch (field)
            {
                case FilterField.Author:
                    AddAuthorFilter();
                    break;

                case FilterField.Country:
                    AddCountryFilter();
                    break;

                case FilterField.Year:
                    AddYearFilter();
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Filter By Author
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilterByAuthor(object sender, FilterEventArgs e)
        {
            // see Notes on Filter Methods:
            var src = e.Item as Thing;
            if (src == null)
                e.Accepted = false;
            else if (string.Compare(SelectedAuthor, src.Author) != 0)
                e.Accepted = false;
        }

        /// <summary>
        /// Filter By Country
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilterByCountry(object sender, FilterEventArgs e)
        {
            // see Notes on Filter Methods:
            var src = e.Item as Thing;
            if (src == null)
                e.Accepted = false;
            else if (string.Compare(SelectedCountry, src.Country) != 0)
                e.Accepted = false;
        }

        /// <summary>
        /// Filter By Year
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilterByYear(object sender, FilterEventArgs e)
        {
            // see Notes on Filter Methods:
            var src = e.Item as Thing;
            if (src == null)
                e.Accepted = false;
            else if (SelectedYear != src.Year)
                e.Accepted = false;
        }

        /// <summary>
        /// This method handles a message recieved from the View which enables a reference to the
        /// instantiated CollectionViewSource to be used in the ViewModel.
        /// </summary>
        private void Handle_ViewCollectionViewSourceMessageToken(ViewCollectionViewSourceMessageToken token)
        {
            Debug.WriteLine("Handle_ViewCollectionViewSourceMessageToken");
            CVS = token.CVS;
            ColumnFilter.ViewSource = CVS;
        }

        private void InitializeCommands()
        {
            ResetFiltersCommand = new RelayCommand(ResetFilters, null);
            RemoveCountryFilterCommand = new RelayCommand(RemoveCountryFilter, () => CanRemoveCountryFilter);
            RemoveAuthorFilterCommand = new RelayCommand(RemoveAuthorFilter, () => CanRemoveAuthorFilter);
            RemoveYearFilterCommand = new RelayCommand(RemoveYearFilter, () => CanRemoveYearFilter);
        }

        private void LoadData()
        {
            Debug.WriteLine("LoadData");
            // Get the global list to be filtered
            var things = DataService.GetThings();

            // get the distinct list of Author
            var q1 = from t in things select t.Author;
            // Set comboBox ItemsSource of distinct Author
            Authors = new ObservableCollection<string>(q1.Distinct());

            var q2 = from t in things select t.Country;
            Countries = new ObservableCollection<string>(q2.Distinct());

            var q3 = from t in things select t.Year;
            Years = new ObservableCollection<int>(q3.Distinct());

            // CollectionViewSource
            Things = new ObservableCollection<Thing>(things);

            var enumerable = from t in things select t.Author;
            ColumnFilter = new ColumnFilter
            {
                FieldName = "Author",
                ItemColumn = new ObservableCollection<string>(enumerable.Distinct())
            };

            Debug.WriteLine("");



        }

        // Command methods (called by the commands) ===============
        // Other helper methods ==============

        /* Notes on Adding Filters:
         *   Each filter is added by subscribing a filter method to the Filter event
         *   of the CVS.  Filters are applied in the order in which they were added.
         *   To prevent adding filters mulitple times ( because we are using drop down lists
         *   in the view), the CanRemove***Filter flags are used to ensure each filter
         *   is added only once.  If a filter has been added, its corresponding CanRemove***Filter
         *   is set to true.
         *
         *   If a filter has been applied already (for example someone selects "Canada" to filter by country
         *   and then they change their selection to another value (say "Mexico") we need to undo the previous
         *   country filter then apply the new one.  This does not completey Reset the filter, just
         *   allows it to be changed to another filter value. This applies to the other filters as well
         */
        /* Notes on Filter Methods:
         * When using multiple filters, do not explicitly set anything to true.  Rather,
         * only hide things which do not match the filter criteria
         * by setting e.Accepted = false.  If you set e.Accept = true, if effectively
         * clears out any previous filters applied to it.
         */
    }
}