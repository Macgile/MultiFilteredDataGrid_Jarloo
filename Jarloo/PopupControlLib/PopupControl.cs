﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PopupControlLib
{
    /// <inheritdoc />
    public class PopupControl : Control
    {

        /// <summary>
        /// Registers a dependency property as backing store for the Content property
        /// </summary>
        //public static readonly DependencyProperty ContentProperty =
        //    DependencyProperty.Register("Content", typeof(object), typeof(PopupControl),
        //        new FrameworkPropertyMetadata(null,
        //            FrameworkPropertyMetadataOptions.AffectsRender |
        //            FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        /// <summary>
        /// Is Open
        /// </summary>
        public static readonly DependencyProperty PopUpIsOpenProperty =
            DependencyProperty.Register("PopUpIsOpen", typeof(bool), typeof(PopupControl), new PropertyMetadata(false));

        /// <inheritdoc />
        /// <summary>
        /// Placement
        /// </summary>
       public PopupControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PopupControl),
                new FrameworkPropertyMetadata(typeof(PopupControl)));
        }

        /// <summary>
        /// Placement
        /// </summary>
        //public PlacementMode Placement
        //{
        //    get { return (PlacementMode)GetValue(PlacementProperty); }
        //    set { SetValue(PlacementProperty, value); }
        //}
        /// <summary>
        /// Is Open
        /// </summary>
        public bool PopUpIsOpen
        {
            get { return (bool) GetValue(PopUpIsOpenProperty); }
            set { SetValue(PopUpIsOpenProperty, value); }
        }


        /// <summary>
        /// Gets or sets the Content.
        /// </summary>
        /// <value>The Content of Popup.</value>
        //public object Content
        //{
        //    get { return (object) GetValue(ContentProperty); }
        //    set { SetValue(ContentProperty, value); }
        //}
    }
}