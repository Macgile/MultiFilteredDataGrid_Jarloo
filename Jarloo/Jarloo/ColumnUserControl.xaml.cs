﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
// ReSharper disable ArrangeAccessorOwnerBody

namespace Jarloo
{
    /// <inheritdoc cref="UserControl" />
    /// <summary>
    /// Logique d'interaction pour ColumnUserControl.xaml
    /// </summary>
    public partial class ColumnUserControl : UserControl
    {

        /// <summary>
        /// Is Open
        /// </summary>
        public static readonly DependencyProperty IsOpenProperty =
            DependencyProperty.Register("IsOpen", typeof(bool), typeof(ColumnUserControl), new PropertyMetadata(false));

        /// <summary>
        /// Is Open
        /// </summary>
        public static readonly DependencyProperty PlacementTargetProperty =
            DependencyProperty.Register("PlacementTarget", typeof(UIElement), typeof(ColumnUserControl), new PropertyMetadata(null));


        public UIElement PlacementTarget
        {
            get { return (UIElement)GetValue(PlacementTargetProperty); }
            set { SetValue(PlacementTargetProperty, value); }
        }


        public ColumnUserControl()
        {
            InitializeComponent();
            DataContext = this;
        }

        /// <summary>
        /// Is Open
        /// </summary>
        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

    }
}
