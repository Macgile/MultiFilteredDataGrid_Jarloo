﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using System.IO;
using System.ComponentModel;
using System.Windows.Data;
using System.Reflection;

// https://kailashsblogs.blogspot.fr/2017/02/wpf-custom-datagrid-controlfilterable.html


namespace Jarloo
{
    public class FilterableDataGrid : DataGrid
    {
        private ICollectionView Datalist { get; set; }
        private object SearchValue { get; set; }
        public string ColumnName { get; set; }
        public bool IsFilter { get; set; }

        public FilterableDataGrid()
        {
            IsFilter = false;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
        }

        protected override void OnItemsSourceChanged(System.Collections.IEnumerable oldValue,
            System.Collections.IEnumerable newValue)
        {
            Datalist = CollectionViewSource.GetDefaultView(newValue);
            base.OnItemsSourceChanged(oldValue, Datalist);
            if (IsFilter)
            {
                foreach (var DGC in Columns)
                {
                    var Factory = new FrameworkElementFactory(typeof(StackPanel));
                    var LFactory = new FrameworkElementFactory(typeof(Label));
                    LFactory.SetValue(ContentControl.ContentProperty, DGC.Header.ToString());
                    Factory.AppendChild(LFactory);
                    if (DGC.GetType().Name == "DataGridTextColumn")
                    {
                        var TFactory = new FrameworkElementFactory(typeof(TextBox));
                        TFactory.SetValue(MarginProperty, new Thickness(0));
                        TFactory.SetValue(WidthProperty, 150.00);
                        TFactory.SetValue(NameProperty, "txt" + DGC.Header);
                        TFactory.AddHandler(TextBoxBase.TextChangedEvent, new TextChangedEventHandler(TextBox_TextChanged),
                            false);
                        Factory.AppendChild(TFactory);
                    }

                    if (DGC.GetType().Name == "DataGridCheckBoxColumn")
                    {
                        var TFactory = new FrameworkElementFactory(typeof(CheckBox));
                        TFactory.SetValue(NameProperty, "txt" + DGC.Header);
                        TFactory.AddHandler(ButtonBase.ClickEvent, new RoutedEventHandler(CheckBox_Checked), false);
                        Factory.AppendChild(TFactory);
                    }

                    var Template = new DataTemplate
                    {
                        DataType = typeof(HeaderedContentControl),
                        VisualTree = Factory
                    };
                    DGC.HeaderTemplate = Template;
                }
            }
        }

        void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var CB = (CheckBox) sender;
            SearchValue = CB.IsChecked;
            var CP = (ContentPresenter) CB.TemplatedParent;
            var DGCH = (DataGridColumnHeader) CP.TemplatedParent;
            var DGC = DGCH.Column;
            ColumnName = DGC.Header.ToString();
            Datalist.Filter = CustomeFilter;
        }

        private void TextBox_TextChanged(object sender, RoutedEventArgs e)
        {
            var STB = (TextBox) sender;
            SearchValue = STB.Text;
            var CP = (ContentPresenter) STB.TemplatedParent;
            var DGCH = (DataGridColumnHeader) CP.TemplatedParent;
            var DGC = DGCH.Column;
            ColumnName = DGC.Header.ToString();
            Datalist.Filter = CustomeFilter;
        }

        private bool CustomeFilter(object item)
        {
            var TP = item.GetType();
            var PI = TP.GetProperty(ColumnName);
            var obj = new object[SearchValue.ToString().Length];
            var values = PI.GetValue(item, obj).ToString();
            values = values.ToUpper();
            return values.StartsWith(SearchValue.ToString().ToUpper());
        }
    }
}