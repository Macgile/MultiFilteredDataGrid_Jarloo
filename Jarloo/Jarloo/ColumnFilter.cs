﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable ConvertToAutoProperty

namespace Jarloo
{
    public class CheckedItem : ViewModelBase
    {
        #region Private Fields

        private bool isChecked;

        #endregion Private Fields

        #region Public Properties

        public string Content { get; set; }
        public int Id { get; set; }

        public bool IsChecked
        {
            get => isChecked;
            set
            {
                isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }

        #endregion Public Properties
    }

    public class ColumnFilter<T> : ViewModelBase
    {
        #region Private Fields

        // List of item to filter
        private bool isCurrentFilter;

        private ObservableCollection<CheckedItem> itemColumn;
        private string searchText;
        private CollectionViewSource viewSource;

        #endregion Private Fields

        #region Private Properties

        private ICollectionView ItemColumnView { get; set; }

        #endregion Private Properties

        #region Protected Properties

        protected string SelectAll { get; set; }

        #endregion Protected Properties

        #region Public Properties

        /// <summary>
        /// Apply filter Command
        /// </summary>
        public ICommand ApplyFilterCommand => new RelayCommand(ApplyFilter, CanApplyFilter);

        // Name of the field to filter
        public string FieldName { get; set; }

        //public bool FilterAction { get; set; }
        public bool IsFiltered { get; private set; }

        // List of items to display in the ListBox
        public ObservableCollection<CheckedItem> ItemColumn
        {
            get => itemColumn;
            set
            {
                if (itemColumn == value) return;
                itemColumn = value;
                OnPropertyChanged("ItemColumn");
            }
        }

        /// <summary>
        /// Remove filter Command
        /// </summary>
        public ICommand RemoveFilterCommand => new RelayCommand(RemoveFilter, () => IsFiltered);

        /// <summary>
        /// Filter search
        /// </summary>
        public string SearchText
        {
            get => searchText;
            set
            {
                if (searchText == value) return;
                searchText = value;
                ItemColumnView.Refresh();
                OnPropertyChanged("SearchText");
            }
        }

        public List<string> SelectedItemsList { get; } = new List<string>();
        public ICommand SortCommand => new RelayCommand<string>(Sorting);
        public string SortDirection { get; set; }

        /// <summary>
        /// Global CollectionViewSource (shared by all filter)
        /// </summary>
        public CollectionViewSource ViewSource
        {
            get => viewSource;
            set
            {
                if (value == null) return;

                viewSource = value;
                OnPropertyChanged("ViewSource");

                viewSource.View.CollectionChanged += ViewOnCollectionChanged;
                // Fill List of items displayed in Filter
                GetItemColumn();
            }
        }

        #endregion Public Properties

        #region Private Methods

        /// <summary>
        /// Get value of T
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static object GetValObj(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName)?.GetValue(obj, null);
        }

        /// <summary>
        /// Apply Filter Command
        /// </summary>
        private void ApplyFilter()
        {
            //Debug.WriteLine("ApplyFilter");
            // if(!FilterAction) return;

            isCurrentFilter = true;

            if (IsFiltered)
            {
                ViewSource.Filter -= Filter;
                ViewSource.Filter += Filter;
            }
            else
            {
                ViewSource.Filter += Filter;
                IsFiltered = true;
            }

            isCurrentFilter = false;
            OnPropertyChanged("IsFiltered");
        }

        /// <summary>
        /// Can Apply Filter
        /// </summary>
        /// <returns></returns>
        private bool CanApplyFilter()
        {
            //Debug.WriteLine($"CanApplyFilter Command {SelectedItemsList.Count}");
            return SelectedItemsList.Count >= 0 && SelectedItemsList.Count < ItemColumn.Count;
        }

        /// <summary>
        /// Filter By item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Filter(object sender, FilterEventArgs e)
        {
            // At this stage,  e.Accepted is set to True
            // Accepted means accepted (visible) in the filter view

            var src = (T)e.Item;

            if (src == null)
            {
                e.Accepted = false;
            }
            else
            {
                var value = GetValObj(e.Item, FieldName).ToString();
                //var result = selectedItemsList.Contains(value, StringComparer.OrdinalIgnoreCase);
                //Debug.WriteLine($"Value : {value}\t List Contain {value} : {result}");

                // IF CONDITION IS TRUE, ACCEPTED IS FALSE (THE ITEM IS FILTERED AND NOT VISIBLE IN RESULT VIEW)
                // the list is the items will be hidden in the result view : this item result true in condition

                // IF TRUE, SET ACCEPTED AS FALSE; (NOT Accepted(HIDDEN) in VIEW FILTERED)
                if (SelectedItemsList.Contains(value, StringComparer.OrdinalIgnoreCase))
                {
                    // Debug.WriteLine($"{value} IS NOT ACCEPTED");
                    e.Accepted = false;
                }

                // if e.Accepted is not set to false, e.Accepted = true by default
            }
        }

        /// <summary>
        /// List of items of fieldName
        /// </summary>
        private void GetItemColumn()
        {
            var items = ViewSource.View.Cast<T>()
                .Select(v => GetValObj(v, FieldName).ToString())
                .Distinct()
                .Select((f, index) => new CheckedItem
                {
                    Id = index + 1,
                    Content = f,
                    IsChecked = true
                })
                .OrderBy(o => o.Id)
                .ToList();

            items.Insert(0, new CheckedItem() { Id = 0, Content = $"({SelectAll})", IsChecked = true });
            ItemColumn = new ObservableCollection<CheckedItem>(items);

            // CollectionView && Filter text
            ItemColumnView = CollectionViewSource.GetDefaultView(ItemColumn);
            ItemColumnView.Filter = SearchFilter;
        }

        private void RemoveSorting()
        {
            var sorting = ViewSource.SortDescriptions
                .Where(s => s.PropertyName == FieldName).ToList();

            //Debug.WriteLine($"Sort Count : {sorting.Count}");

            foreach (var sort in sorting)
            {
                //Debug.WriteLine($"Remove Sorting : {sort.PropertyName} Direction {sort.Direction.ToString()}");
                ViewSource.SortDescriptions.Remove(sort);
            }
        }

        private bool SearchFilter(object obj)
        {
            var item = (CheckedItem)obj;
            if (string.IsNullOrEmpty(searchText) || item == null || item.Id == 0) return true;

            var result = item.Content.IndexOf(searchText, StringComparison.OrdinalIgnoreCase);

            // Debug.WriteLine($"Item : {item.Content} Containt '{searchText}' : {result}");

            return result >= 0;
        }

        private void Sorting(string direction)
        {

            //Debug.WriteLine($"SortDirection: {SortDirection} Parameter : {direction}");
            RemoveSorting();

            if (direction == "Asc" && SortDirection != direction)
            ViewSource.SortDescriptions.Add(new SortDescription(FieldName, ListSortDirection.Ascending));

            if (direction == "Desc" && SortDirection != direction)
                ViewSource.SortDescriptions.Add(new SortDescription(FieldName, ListSortDirection.Descending));

            SortDirection = direction == SortDirection ? "" : direction;
            OnPropertyChanged("SortDirection");

            //var sorting = ViewSource.SortDescriptions
            //    .Where(s => s.PropertyName == FieldName).ToList();

            //Debug.WriteLine($"Sort {FieldName} Count : {sorting.Count} Global Count : {ViewSource.SortDescriptions.Count}");


        }

        /// <summary>
        /// Parent ViewOnCollection Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!isCurrentFilter)
            {
                // List of items of fieldName
                GetItemColumn();
            }
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Remove Filter
        /// </summary>
        public void RemoveFilter()
        {
            ViewSource.Filter -= Filter;
            IsFiltered = false;
            SelectedItemsList.Clear();
            OnPropertyChanged("IsFiltered");
        }

        #endregion Public Methods
    }

    public class FilterHeader : ColumnFilter<Customer>
    {
        #region Public Constructors

        public FilterHeader()
        {
            SelectAll = "Sélectionner tout";
        }

        #endregion Public Constructors
    }
}