﻿using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
#pragma warning disable 1574

// ReSharper disable FieldCanBeMadeReadOnly.Local
// ReSharper disable ArrangeAccessorOwnerBody

namespace Jarloo
{
    /// <inheritdoc cref="UserControl" />
    /// <summary>
    /// Logique d'interaction pour DatagridHeader.xaml
    /// </summary>
    public sealed partial class DatagridHeader
    {
        #region Private Fields

        private Cursor cursor;
        private double minHeight;
        private double minWidth;

        #endregion Private Fields

        #region Public Fields

        /// <summary>
        /// Define FilterHeader Property
        /// </summary>
        public static readonly DependencyProperty DataFilterProperty =
            DependencyProperty.Register("DataFilter", typeof(FilterHeader), typeof(DatagridHeader),
                new UIPropertyMetadata(null));

        /// <summary>
        /// Define Header Property
        /// </summary>
        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(string), typeof(DatagridHeader),
                new PropertyMetadata(default(string)));

        #endregion Public Fields

        #region Private Properties

        private double SizableContentHeight { get; set; }

        private double SizableContentWidth { get; set; }

        #endregion Private Properties

        #region Public Properties

        /// <summary>
        /// Set or Get FilterHeader Property
        /// </summary>
        public FilterHeader DataFilter
        {
            get { return (FilterHeader) GetValue(DataFilterProperty); }
            set { SetValue(DataFilterProperty, value); }
        }

        /// <summary>
        ///  Set or Get Header Text
        /// </summary>
        public string Header
        {
            get { return (string) GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        #endregion Public Properties

        #region Public Constructors

        /// <inheritdoc />
        /// <summary>
        /// Constructor, at this stage, the DataFilter instance is not initalized
        /// </summary>
        public DatagridHeader()
        {
            InitializeComponent();
            minHeight = SizableContent.MinHeight;
            minWidth = SizableContent.MinWidth;
            //DataContext = this;
        }

        #endregion Public Constructors

        #region Private Methods

        private void CheckBoxStateChanged(object sender, RoutedEventArgs e)
        {
            var item = (CheckBox) sender;
            // if nul or if in indeterminate state
            if (item?.IsChecked == null || item.DataContext.GetType() != typeof(CheckedItem)) return;

            var content = item.Content.ToString();
            var data = (CheckedItem) item.DataContext;

            if (data.Id != 0)
            {
                // if list not contain content AND item is nok checked
                if (!DataFilter.SelectedItemsList.Contains(content) && (bool) !item.IsChecked)
                    DataFilter.SelectedItemsList.Add(content);

                // if list contain content AND item is checked (hidde item in view filtered)
                if (DataFilter.SelectedItemsList.Contains(content) && (bool) item.IsChecked)
                    DataFilter.SelectedItemsList.Remove(content);
            }
            else
            {
                // TODO : manage The filter delete button because it is always enabled or in an inconsistent state
                // state of "select all" checkbox
                var state = (bool) item.IsChecked;
                // if un checked un select all, the button Ok is disable (the filter work with one element min)
                // if checked entire selected all element are displayed (nothingh is filtered)

                var checkItems = (from CheckedItem li in ListItems.Items
                    //where li.IsChecked != state
                    select li).ToList();

                foreach (var checkItem in checkItems)
                {
                    checkItem.IsChecked = state;

                    if (!DataFilter.SelectedItemsList.Contains(checkItem.Content) && !state)
                        DataFilter.SelectedItemsList.Add(checkItem.Content);

                    if (DataFilter.SelectedItemsList.Contains(checkItem.Content) && state)
                        DataFilter.SelectedItemsList.Remove(checkItem.Content);
                }
            }

            // check the state filter if change ????
            //Debug.WriteLine($"Content : {item.Content} Is Checked : {item.IsChecked} SelectedItemsList.Count: {DataFilter.SelectedItemsList.Count}");
        }

        private void ClearSearchBox_OnClick(object sender, RoutedEventArgs e)
        {
            DataFilter.SearchText = null;
        }

        /// <summary>
        /// On Resize Thumb Drag Completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnResizeThumbDragCompleted(object sender, DragCompletedEventArgs e)
        {
            Cursor = cursor;
        }

        // https://stackoverflow.com/questions/3685566/wpf-using-resizegrip-to-resize-controls
        private void OnResizeThumbDragDelta(object sender, DragDeltaEventArgs e)
        {
            // initialize the first Actual size Width/Height
            if (SizableContentHeight <= 0)
            {
                SizableContentHeight = SizableContent.ActualHeight;
                SizableContentWidth = SizableContent.ActualWidth;
            }

            var yAdjust = SizableContent.Height + e.VerticalChange;
            var xAdjust = SizableContent.Width + e.HorizontalChange;

            //make sure not to resize to negative width or heigth
            xAdjust = SizableContent.ActualWidth + xAdjust > minWidth ? xAdjust : minWidth;
            yAdjust = SizableContent.ActualHeight + yAdjust > minHeight ? yAdjust : minHeight;

            // set the size of grid
            SizableContent.Width = xAdjust;
            SizableContent.Height = yAdjust;
        }

        /// <summary>
        /// On Resize Thumb DragStarted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnResizeThumbDragStarted(object sender, DragStartedEventArgs e)
        {
            cursor = Cursor;
            Cursor = Cursors.SizeNWSE;
        }

        /// <summary>
        /// Reset the size of popup to original size
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PopupTemplate_Closed(object sender, System.EventArgs e)
        {
            SizableContent.Width = SizableContentWidth;
            SizableContent.Height = SizableContentHeight;
        }

        private void Toggle_OnClick(object sender, RoutedEventArgs e)
        {
            //Debug.WriteLine("Toggle_OnClick");
            ToggleButtonFilter.IsChecked = !ToggleButtonFilter.IsChecked;
            DataFilter.SearchText = null;
        }

        #endregion Private Methods
    }
}