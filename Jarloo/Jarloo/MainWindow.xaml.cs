﻿/*
    Jarloo
    http://www.jarloo.com

    This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License
    http://creativecommons.org/licenses/by-sa/3.0/

*/

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

// http://www.jarloo.com/excel-like-autofilter-in-wpf/

namespace Jarloo
{
    public partial class MainWindow : Window
    {
        #region Private Fields

        private readonly ObservableCollection<Customer> customers = new ObservableCollection<Customer>();
        private readonly CollectionViewSource viewSource = new CollectionViewSource();

        #endregion Private Fields

        #region Public Properties

        public FilterHeader AgeFilter { get; set; }
        public FilterHeader AuthorFilter { get; set; }
        public FilterHeader CountryFilter { get; set; }

        //public ObservableCollection<Customer> Customers { get; set; }
        // itemSource datagrid
        public ICollectionView ViewSource { get; set; }

        #endregion Public Properties

        #region Public Constructors

        public MainWindow()
        {
            InitializeComponent();

            //Make sample data
            customers.Add(new Customer() {Name = "John Smith"       , Country = "CA", Age = 10});
            customers.Add(new Customer() {Name = "John Whane"       , Country = "US", Age = 10});
            customers.Add(new Customer() {Name = "John Smith"       , Country = "FR", Age = 30});
            customers.Add(new Customer() {Name = "Jake Shields"     , Country = "US", Age = 10});
            customers.Add(new Customer() {Name = "Shelly Brown"     , Country = "AU", Age = 20});
            customers.Add(new Customer() {Name = "Sidney Wright"    , Country = "CA", Age = 30});
            customers.Add(new Customer() {Name = "Bart Simpson"     , Country = "US", Age = 40});
            customers.Add(new Customer() {Name = "Jery Smith"       , Country = "FR", Age = 25});
            customers.Add(new Customer() {Name = "Paul Shields"     , Country = "BE", Age = 10});
            customers.Add(new Customer() {Name = "John Brown"       , Country = "AU", Age = 26});
            customers.Add(new Customer() {Name = "Sidney Black"     , Country = "CH", Age = 11});
            customers.Add(new Customer() {Name = "Homer Simpson"    , Country = "US", Age = 34});
            customers.Add(new Customer() {Name = "Sidney Yellow"    , Country = "CA", Age = 30});
            customers.Add(new Customer() {Name = "Holly Simpson"    , Country = "US", Age = 40});
            customers.Add(new Customer() {Name = "Francky Smith"    , Country = "FR", Age = 25});
            customers.Add(new Customer() {Name = "James Shields"    , Country = "BE", Age = 10});
            customers.Add(new Customer() {Name = "John Dean"        , Country = "AU", Age = 26});
            customers.Add(new Customer() {Name = "Sidney Polack"    , Country = "CH", Age = 11});
            customers.Add(new Customer() {Name = "Homer White"      , Country = "US", Age = 34});


            //Customers = customers;
            viewSource.Source = customers.OrderBy(o=>o.Name);
            ViewSource = viewSource.View;

            AuthorFilter = new FilterHeader
            {
                FieldName = "Name",
                ViewSource = viewSource
            };

            CountryFilter = new FilterHeader
            {
                FieldName = "Country",
                ViewSource = viewSource
            };

            AgeFilter = new FilterHeader
            {
                FieldName = "Age",
                ViewSource = viewSource
            };

            DataContext = this;
        }

        #endregion Public Constructors

        void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex()).ToString();
        }
    }
}