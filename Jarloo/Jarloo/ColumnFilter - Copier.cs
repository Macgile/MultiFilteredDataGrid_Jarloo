﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using Jarloo;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable ConvertToAutoProperty

namespace Toto
{
    public class FilterHeader : ColumnFilter<Customer>
    {
    }

    public class CheckedItem
    {
        public string Content { get; set; }
        public bool IsChecked { get; set; }
    }


    public class ColumnFilter<T> : ViewModelBase
    {
        #region Private Fields

        private bool canRemoveFilter;
        private bool isCurrentFilter;

        private ObservableCollection<string> itemColumn;
        private ICommand removeFilterCommand;
        private ICommand applyFilterCommand;
        private string selectedItem;
        private CollectionViewSource viewSource;

        private readonly List<string> selectedItemsList = new List<string>();

        #endregion Private Fields

        #region Public Properties

        public bool IsFiltered => canRemoveFilter;

        // Field name of filter
        public string FieldName { get; set; }

        // list of item displayed in ListBox
        public ObservableCollection<string> ItemColumn
        {
            get => itemColumn;
            set
            {
                if (itemColumn == value) return;
                itemColumn = value;
                OnPropertyChanged("ItemColumn");
            }
        }

        /// <summary>
        /// Remove filter Command
        /// </summary>
        public ICommand RemoveFilterCommand
            => removeFilterCommand ??
               (removeFilterCommand = new RelayCommand(RemoveFilter, () => canRemoveFilter));

        /// <summary>
        /// Apply filter Command
        /// </summary>
        public ICommand ApplyFilterCommand
            => applyFilterCommand ??
               (applyFilterCommand = new RelayCommand(ApplyFilter, CanApplyFilter));

        /// <summary>
        /// Can Apply Filter if SelectedItemsList count > 0
        /// </summary>
        /// <returns></returns>
        private bool CanApplyFilter()
        {
            //Debug.WriteLine($"CanApplyFilter Command {SelectedItemsList.Count}");
            return SelectedItemsList.Count > 0;
        }

        /// <summary>
        /// Apply Filter Command
        /// </summary>
        private void ApplyFilter()
        {
            //Debug.WriteLine("ApplyFilter Command");

            isCurrentFilter = true;
            // see Notes on Adding Filters:
            if (canRemoveFilter)
            {
                // Debug.WriteLine($"{FieldName.ToUpper()} START => canRemoveFilter");

                ViewSource.Filter -= Filter;
                ViewSource.Filter += Filter;
                // Debug.WriteLine($"{FieldName.ToUpper()} END => canRemoveFilter");
            }
            else
            {
                // Debug.WriteLine($"{FieldName.ToUpper()} START => AddFilter");
                // isCurrentFilter = true;

                ViewSource.Filter += Filter;
                canRemoveFilter = true;

                // Debug.WriteLine($"{FieldName.ToUpper()} END => AddFilter\r\n");
            }

            isCurrentFilter = false;
            OnPropertyChanged("IsFiltered");
        }

        public List<string> SelectedItemsList => selectedItemsList;

        //  current selected item in combobox list
        public string SelectedItem
        {
            get => selectedItem;
            set
            {
                if (selectedItem == value) return;
                selectedItem = value;
                OnPropertyChanged("SelectedItem");

                if (selectedItem == null) return;
                AddFilter();
            }
        }

        /// <summary>
        /// Global CollectionViewSource (shared by all filter)
        /// </summary>
        public CollectionViewSource ViewSource
        {
            get => viewSource;
            set
            {
                if (value == null) return;

                viewSource = value;
                OnPropertyChanged("ViewSource");

                viewSource.View.CollectionChanged += ViewOnCollectionChanged;
                // List of items of fieldName
                GetItemColumn();
            }
        }

        #endregion Public Properties

        #region Public Methods

        public void AddFilter()
        {
            // // Debug.WriteLine($"\r\n{FieldName.ToUpper()} : canRemoveFilter:{canRemoveFilter}");

            isCurrentFilter = true;
            // see Notes on Adding Filters:
            if (canRemoveFilter)
            {
                // Debug.WriteLine($"{FieldName.ToUpper()} START => canRemoveFilter");

                ViewSource.Filter -= Filter;
                ViewSource.Filter += Filter;
                // Debug.WriteLine($"{FieldName.ToUpper()} END => canRemoveFilter");
            }
            else
            {
                // Debug.WriteLine($"{FieldName.ToUpper()} START => AddFilter");
                // isCurrentFilter = true;

                ViewSource.Filter += Filter;
                canRemoveFilter = true;

                // Debug.WriteLine($"{FieldName.ToUpper()} END => AddFilter\r\n");
            }

            isCurrentFilter = false;
        }

        /// <summary>
        /// Remove Filter
        /// </summary>
        public void RemoveFilter()
        {
            //Debug.WriteLine($"{FieldName.ToUpper()} RemoveFilter");
            ViewSource.Filter -= Filter;
            ViewSource.Filter -= Filter;
            SelectedItem = null;
            canRemoveFilter = false;
            selectedItemsList.Clear();
            OnPropertyChanged("IsFiltered");
        }

        #endregion Public Methods

        #region Private Methods

        private static object GetValObj(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName)?.GetValue(obj, null);
        }

        /// <summary>
        /// Filter By item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Filter(object sender, FilterEventArgs e)
        {
            //// Debug.WriteLine("Filter");

            // see Notes on Filter Methods:
            var src = (T) e.Item;

            if (src == null)
                e.Accepted = false;

            // get value of field by FieldName : src.Name
            else if (string.CompareOrdinal(SelectedItem, GetValObj(e.Item, FieldName).ToString()) != 0)
                e.Accepted = false;
        }

        /// <summary>
        /// Filter By item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Filter(object sender, FilterEventArgs e)
        {
            // At this stage,  e.Accepted is set to True
            // Accepted means accepted (visible) in the filter view

            var src = (T) e.Item;

            if (src == null)
            {
                e.Accepted = false;
            }
            else
            {
                var value = GetValObj(e.Item, FieldName).ToString();
                //var result = selectedItemsList.Contains(value, StringComparer.OrdinalIgnoreCase);
                //Debug.WriteLine($"Value : {value}\t List Contain {value} : {result}");

                // IF CONDITION IS TRUE, ACCEPTED IS FALSE (THE ITEM IS FILTERED AND NOT VISIBLE IN RESULT VIEW)
                // the list is the items will be hidden in the result view : this item result true in condition

                // IF TRUE, SET ACCEPTED AS FALSE; (NOT Accepted(HIDDEN) in VIEW FILTERED)
                if (selectedItemsList.Contains(value, StringComparer.OrdinalIgnoreCase))
                {
                    // Debug.WriteLine($"{value} IS NOT ACCEPTED");
                    e.Accepted = false;
                }

                // if e.Accepted is not set to false, e.Accepted = true by default
            }
        }

        // List of items of fieldName
        private void GetItemColumn()
        {
            var items = ViewSource.View.Cast<T>()
                .Select(f => GetValObj(f, FieldName).ToString())
                .Distinct()
                .ToList();
            items.Insert(0, "(Sélectionner tout)");
            ItemColumn = new ObservableCollection<string>(items);
        }

        // Parent ViewOnCollection Changed
        private void ViewOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // Debug.WriteLine($"FieldName : {FieldName.ToUpper()} isCurrentFilter: {isCurrentFilter}");
            // Debug.WriteLine($"Action:{e.Action}\tNewItems:{e.NewItems}\tNewStartingIndex:{e.NewStartingIndex}\tOldItems:{e.OldItems}");

            if (!isCurrentFilter)
            {
                // List of items of fieldName
                GetItemColumn();
            }
        }

        #endregion Private Methods
    }
}